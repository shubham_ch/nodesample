var userModal = require('../models/userModel');

exports.listUsers = async (req, res) => {
	var query = (req.params.searchTerm)?{ name: { $regex: req.params.searchTerm, $options: 'i' } }:{};
	var pageNumber = (req.params.pageNumber)?parseInt(req.params.pageNumber):1;
	var skipVal = 5 * (pageNumber - 1);

	await userModal.find(query, {}, { skip: skipVal, limit: 5 }, (err, user) => {
		if (err)
			res.json({ status: 500, data: [], message: err });
		res.json({ status: 200, data: user, message: "List of users" });
	});
};

exports.createUsers = async (req, res) => {
	var newUsersList = [];
	req.body.users.forEach(async (user, index) => {
		let isExists = false;
		await userModal.find({ email: user.email }, (err, existUser) => {
			if (err)
				res.json({ status: 500, data: [], message: err });
			if(existUser.length > 0)
				isExists = true;
		});
		
		if (!isExists) {
			newUsersList.push(user)
		}
		
		if(index === (req.body.users.length - 1)) {
			userModal.insertMany(newUsersList).then((newUsers) => {
				res.json({ status: 201, data: newUsers, message: "List of new created users" });
			}).catch((err) => {
				res.json({ status: 500, data: [], message: err });
			});
		}
	})
};

exports.getUser = async (req, res) => {
	await userModal.findById(req.params.userId, (err, user) => {
		if (err)
			res.json({ status: 500, data: [], message: err });
		res.json({ status: 200, data: user, message: "User details" });
	});
};

exports.updateUser = async (req, res) => {
	await userModal.findOneAndUpdate({ _id: req.params.userId }, req.body, (err, user) => {
		if (err)
			res.json({ status: 500, data: [], message: err });
		res.json({ status: 200, data: user, message: "User details updated" });
	});
};

exports.deleteUser = async (req, res) => {
	await userModal.remove({
		_id: req.params.userId
	}, (err, user) => {
		if (err)
			res.json({ status: 500, data: [], message: err });
		res.json({ status:200, data: [], message: 'User successfully deleted' });
	});
};
