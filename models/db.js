var mongoose = require('mongoose');

mongoose.connect(process.env.MONGODB_URI,(err) =>{
    if(!err){
        console.log('Mongodb connected');
    }
    else{
        console.log('Mongodb connection failed');
    }
});
