var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema = new Schema({
	name:  { type: String, required: true },
	email: { type: String, required: true, unique: true },
	created_date: { type: Date, default: Date.now },
	status: { type: Boolean, default: true }
});

module.exports = mongoose.model('User', UserSchema);