require('./config/config');
require('./models/db');

const express = require('express');
const bodyParser = require('body-parser');
var router = require('./routes/userRoutes');

// Initialize the express app
var app = express();

// Middlewares
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Initialize the express route instance
app.use('/', router);

app.listen(process.env.PORT, () => {
	console.log("Server started at port :", process.env.PORT)
});