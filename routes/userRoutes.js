const express = require('express');
const router = express.Router();

const userController = require('../controllers/userController');

// Users Routes
router.route('/users/:pageNo/:searchTerm?')
	.get(userController.listUsers)
	.post(userController.createUsers);

router.route('/user/:userId')
	.get(userController.getUser)
	.put(userController.updateUser)
	.delete(userController.deleteUser);

module.exports = router;